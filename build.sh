#!/bin/bash
set -ex

# Client
make -C src/client
[ -f "client.conf" ] || cp example.client.conf client.conf
cp src/client/client ./client

# Server
[ -d ".env" ] || python3 -m venv .env
[ -f "server.yaml" ] || cp example.server.yaml server.yaml 
./.env/bin/python -m pip install wheel
./.env/bin/python -m pip install -r src/server/requirements.txt
[ -f "cert.pem" ] || openssl req -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" -nodes -new -x509 -out cert.pem -keyout cert.key
cat > server <<'EOF'
$(dirname "$0")/.env/bin/python $(dirname "$0")/src/server/main.py
EOF
chmod +x server
