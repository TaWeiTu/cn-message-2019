import os
from pathlib import Path
import yaml

PROJ_ROOT = Path(os.path.dirname(os.path.abspath(__file__))).parent.parent

assert os.path.isfile(os.path.join(PROJ_ROOT, "server.yaml")), "server.yaml does not exist!!!"

with open(os.path.join(PROJ_ROOT, "server.yaml"), "r") as f:
	data = yaml.load(f, Loader=yaml.FullLoader)

SERVER_PORT = data["port"]
DB_PATH = data["db_path"]
