import base64
import datetime
import queue
import socket
import threading
from time import sleep

from db import Session, User, Message, Lastseen, Chat, Participation, Attachment
from db.utils import get_user_by_username, get_user_list_by_chat_id, \
					 get_chat_list_by_user_id, get_message_by_chat_id, \
					 get_user_list, get_attachment_content_by_attachment_id, \
					 get_last_ack_id_by_chat_id_user_id, get_unread_chat_list_by_user_id
from logger import log
from utils import sha256

class Worker(threading.Thread):
	def init_queue(self):
		self.glb[self.user_id] = {"queue":queue.Queue(), "lock":threading.Lock()}

	def register(self, username, password):
		user = get_user_by_username(username)
		if user:
			return ["login_res", "0", "1"]
		session = Session()
		user = User(username=username, password=sha256(password), 
		            last_login=datetime.datetime.now())
		session.add(user)
		session.commit()
		self.user_id = user.user_id
		self.init_queue()
		return ["login_res", str(self.user_id), "0"]

	def login(self, username, password):
		user = get_user_by_username(username)
		if not user or user.password != sha256(password):
			return ["login_res", "0", "1"]
		session = Session()
		user.last_login = datetime.datetime.now()
		session.add(user)
		session.commit()
		self.user_id = user.user_id
		self.init_queue()
		return ["login_res", str(self.user_id), "0"]
	
	def get_users(self):
		users = get_user_list()
		ret = ["user_list"]
		for u in users:
			ret += [str(u.user_id), u.username]
		return ret

	def get_messages(self, chat_id):
		return ["message_list", str(chat_id), str(get_last_ack_id_by_chat_id_user_id(chat_id, self.user_id))] + [' '.join([str(msg.sender_id), 
											 str(msg.message_id), msg.text, 
											 ("attach" if msg.message_type else "text")]) 
								   for msg in get_message_by_chat_id(chat_id)]

	def get_chats(self, from_id=None):
		if from_id is None:
			from_id = self.user_id
		chats = get_chat_list_by_user_id(self.user_id)
		ret = ["chat_list", str(from_id)]
		for c in chats:
			ret += [str(c.chat_id), c.chat_name]
		return ret
	
	def get_unread_chats(self):
		return ["unread_chat_list"] + [ str(x) for x in get_unread_chat_list_by_user_id(self.user_id) ]

	def create_chat(self, chat_name, *user_ids):
		session = Session()
		chat = Chat(chat_name=chat_name)
		session.add(chat)
		session.commit()
		user_ids = [int(x) for x in list(user_ids)]
		user_ids.append(self.user_id)
		user_ids = list(set(user_ids))
		for user_id in user_ids:
			part = Participation(chat_id = chat.chat_id, user_id = user_id)
			session.add(part)
		session.commit()
		for user_id in user_ids:
			if user_id in self.glb:
				self.glb[user_id]["lock"].acquire()
				self.glb[user_id]["queue"].put("update_chat_list " + str(self.user_id))
				self.glb[user_id]["lock"].release()
		
	def send_message(self, chat_id, text, message_type="text"):
		session = Session()
		msg = Message(sender_id=self.user_id, chat_id=chat_id, 
					  time=datetime.datetime.now(), text=text, 
					  message_type=(message_type == "attach"))
		session.add(msg)
		session.commit()
		for user in get_user_list_by_chat_id(chat_id):
			if user.user_id in self.glb:
				self.glb[user.user_id]["lock"].acquire()
				self.glb[user.user_id]["queue"].put(' '.join(list(map(str, ["recv_message", 
														   self.user_id, chat_id,
														   msg.message_id, 
													 	   text, message_type])))
												 + "\n")
				self.glb[user.user_id]["lock"].release()
	
	def send_file(self, username, password, chat_id, attachment_name, content):
		user = get_user_by_username(username)
		if not user or user.password != sha256(password):
			return
		session = Session()
		attach = Attachment(attachment_name=attachment_name, content=content)
		session.add(attach)
		session.commit()
		text = str(attach.attachment_id) + ":" + attachment_name
		self.user_id = user.user_id
		self.send_message(chat_id, text, "attach")
		self.user_id = None
		self.client.send("file_ack\n".encode("utf-8"))
	
	def get_file(self, username, password, attachment_id):
		user = get_user_by_username(username)
		if not user or user.password != sha256(password):
			return
		content = get_attachment_content_by_attachment_id(attachment_id)
		if content is None:
			content = ";"
		self.client.setblocking(1)
		self.client.send((content + "\n").encode("utf-8"))
		self.client.setblocking(0)

	def message_ack(self, chat_id, message_id):
		session = Session()
		msg = Lastseen(user_id=self.user_id, chat_id=chat_id, 
					   message_id=message_id)
		session.add(msg)
		session.commit()
		
	def __init__(self, glb, client):
		threading.Thread.__init__(self)
		self.glb = glb
		self.client = client
		self.user_id = None

	def send_to_client(self, lst):
		self.client.send((' '.join(lst) + "\n").encode("utf-8"))
		
	def process(self, command):
		log("[LOG] process", (command if len(command) <= 150 else command[:150] + "..."))
		commands = command.strip().split(' ')
		if self.user_id is None and commands[0] not in ["register", "login", "send_file", "get_file"]:
			log("[ERROR] process", "not authorized")
			return
		if commands[0] in ["register", "login", "get_users", "get_chats", "get_messages", "get_unread_chats"]:
			res = getattr(self, commands[0])(*commands[1:])
			self.send_to_client(res)
		elif commands[0] in ["send_message", "create_chat", "message_ack", "send_file", "get_file"]:
			getattr(self, commands[0])(*commands[1:])
		else:
			log("[ERROR] process", "unknown command")
		Session().close()

	def receive_message(self):
		try:
			msg = self.client.recv(16777216)
			return msg.decode("utf-8") if msg else None
		except socket.error as e:
			return ""
		
	def run(self):
		self.client.setblocking(0)
		buf = []
		while True:
			msg = self.receive_message()
			if msg is None:
				break
			l = msg.split("\n")
			for s in l[:-1]:
				buf.append(s)
				cmd = "".join(buf)
				self.process(cmd)
				buf = []
			if len(l):
				buf.append(l[-1])

			if self.user_id is not None and not self.glb[self.user_id]["queue"].empty():
				self.glb[self.user_id]["lock"].acquire()
				while not self.glb[self.user_id]["queue"].empty():
					e = self.glb[self.user_id]["queue"].get(block=False)
					if e.startswith("update_chat_list"): 
						self.send_to_client(self.get_chats(e.split()[1]))
					else:
						self.client.send(e.encode("utf-8"))
				self.glb[self.user_id]["lock"].release()
			sleep(0.001)

		if self.user_id is not None:
			del self.glb[self.user_id]

		self.client.close()
