import base64 as b64
import hashlib

def sha256(data):
	s = hashlib.sha256()
	s.update(data.encode("utf-8"))
	h = s.hexdigest()
	return h

def base64(data):
	return b64.b64encode(data.encode("utf-8")).decode("utf-8")
