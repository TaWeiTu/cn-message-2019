import socket
import ssl
import os

from __init__ import SERVER_PORT, PROJ_ROOT
from logger import log
from worker import Worker

def build_socket():
	skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = socket.gethostname()
	port = SERVER_PORT
	skt.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	skt.bind(('', port))
	skt.listen(7122) # 7122 connections
	context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
	context.load_cert_chain(os.path.join(PROJ_ROOT, "cert.pem"), os.path.join(PROJ_ROOT, "cert.key")) 
	return context.wrap_socket(skt, server_side=True)

ssl_skt = build_socket()
world_map = dict()

log("Start listening on port {}".format(SERVER_PORT))

while True:
	try:
		c, addr = ssl_skt.accept()
		log("[LOG] new connection")
		Worker(world_map, c).start()
	except socket.error as e:
		log("[ERROR]", e)


