
from datetime import datetime
import sys

def log(*args):
	print(str(datetime.now()) + ":", *args, file=sys.stderr)

