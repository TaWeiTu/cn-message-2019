from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

import os
from __init__ import PROJ_ROOT, DB_PATH

engine = create_engine("sqlite:///" + os.path.join(PROJ_ROOT, DB_PATH))
Session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()
