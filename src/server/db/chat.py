
from . import Base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime

class Chat(Base):
	__tablename__ = "chats"

	# chat ID
	chat_id = Column(Integer, primary_key=True)

	# username
	chat_name = Column(String, nullable=False)
