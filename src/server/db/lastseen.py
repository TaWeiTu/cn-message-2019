
from . import Base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime

class Lastseen(Base):
	__tablename__ = "lastseen"

	# seen ID
	id = Column(Integer, primary_key=True)

	# user ID
	user_id = Column(Integer, ForeignKey("users.user_id"), primary_key=False)

	# chat ID
	chat_id = Column(Integer, ForeignKey("chats.chat_id"), primary_key=False)

	# message ID
	message_id = Column(Integer, ForeignKey("messages.message_id"), primary_key=False)
