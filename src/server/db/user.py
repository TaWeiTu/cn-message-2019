from . import Base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime

class User(Base):
	__tablename__ = "users"

	# user ID
	user_id = Column(Integer, primary_key=True)

	# last login time
	last_login = Column(DateTime, nullable=False)

	# username
	username = Column(String, nullable=False)

	# password
	password = Column(String, nullable=False)

