
from . import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean

class Message(Base):
	__tablename__ = "messages"

	# message ID
	message_id = Column(Integer, primary_key=True)

	# sender UID
	sender_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

	# chat ID
	chat_id = Column(Integer, ForeignKey("chats.chat_id"), nullable=False)

	# time
	time = Column(DateTime, nullable=False)

	# text of message (filename or attach id (string))
	text = Column(String, nullable=False)

	# message type (False for text, True for attach, default text)
	message_type = Column(Boolean, nullable=False, default=False)


