from .db import Base, Session, engine
from .message import Message
from .user import User
from .chat import Chat
from .lastseen import Lastseen
from .participation import Participation
from .attachment import Attachment

Base.metadata.create_all(engine)
