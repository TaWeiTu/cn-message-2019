from . import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean

class Attachment(Base):
	__tablename__ = "attachments"

	# attachment ID
	attachment_id = Column(Integer, primary_key=True)

	# attachment name
	attachment_name = Column(String, nullable=False)

	# attachment content
	content = Column(String, nullable=False)
