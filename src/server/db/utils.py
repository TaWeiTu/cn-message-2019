from . import Session, User, Chat, Message, Lastseen, Participation, Attachment
from sqlalchemy.sql import func
from sqlalchemy import and_

def get_user_by_username(username):
	s = Session()
	return s.query(User).filter_by(username=username).first()

def get_chat_by_chat_id(cid):
	s = Session()
	return s.query(Chat).get(cid)

def get_chat_list_by_user_id(uid):
	s = Session()
	part_chat_list = s.query(Participation, Chat). \
		filter(Participation.user_id==uid). \
		filter(Participation.chat_id==Chat.chat_id).all()
	return [ x[1] for x in part_chat_list ]

def get_user_list_by_chat_id(cid):
	s = Session()
	part_user_list = s.query(Participation, User). \
		filter(Participation.chat_id==cid). \
		filter(Participation.user_id==User.user_id).all()
	return [ x[1] for x in part_user_list ]

def get_message_by_chat_id(cid):
	s = Session()
	return s.query(Message).filter_by(chat_id=cid).all()

def get_user_list():
	s = Session()
	return s.query(User).all()

def get_attachment_content_by_attachment_id(aid):
	s = Session()
	attach = s.query(Attachment).get(aid) 
	if attach:
		return attach.content
	else:
		return None

def get_last_ack_id_by_chat_id_user_id(cid, uid):
	s = Session()
	ret = s.query(func.max(Lastseen.message_id)).filter(and_(Lastseen.user_id==uid, Lastseen.chat_id==cid)).scalar()
	if ret is None:
		ret = 0
	return ret

def get_unread_chat_list_by_user_id(uid):
	s = Session()
	chat_last_message = s.query(Message.chat_id, func.max(Message.message_id)).group_by(Message.chat_id).all()
	chat_last_message_dict = {}
	for chat_id, last_message_id in chat_last_message:
		chat_last_message_dict[chat_id] = last_message_id
	chats = get_chat_list_by_user_id(uid)
	ret = []
	for c in chats:
		if c.chat_id in chat_last_message_dict and get_last_ack_id_by_chat_id_user_id(c.chat_id, uid) < chat_last_message_dict[c.chat_id]:
			ret.append(c.chat_id)
	return ret
