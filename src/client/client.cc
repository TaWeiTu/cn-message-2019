#include "client.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <resolv.h>
#include <signal.h>
#include <sys/socket.h>
#include <termios.h>
#include <unistd.h>

#include <array>
#include <cstring>
#include <fstream>
#include <iostream>
#include <mutex>
#include <sstream>
#include <string>
#include <utility>

#include "third_party/picosha2.h"

namespace {

std::string domain_to_ip(std::string s){
  addrinfo hints, *host_info;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  int result = getaddrinfo(s.c_str(), NULL, &hints, &host_info);
  if (result != 0)
    return "";
  char host_ip[INET_ADDRSTRLEN + 2];
  memset(host_ip, 0, sizeof(host_ip));
  getnameinfo(host_info->ai_addr, host_info->ai_addrlen, host_ip, sizeof(host_ip), NULL, 0, NI_NUMERICHOST);
  return std::string(host_ip);
}

std::pair<uint16_t, std::string> ReadConfig() {
  std::ifstream ifs("./client.conf");
  if (!ifs) {
    std::cerr << "Config not found\n";
    exit(1);
  }
  std::string domain;
  ifs >> domain;
  uint16_t port;
  ifs >> port;
  return std::make_pair(port, domain_to_ip(domain));
}

}  // namespace

const uint16_t kServerPort = ReadConfig().first;
const in_addr_t kServerIP = inet_addr(ReadConfig().second.c_str());

namespace cli {
namespace {

void SetStdinEcho(bool enable) {
  struct termios tty;
  tcgetattr(STDIN_FILENO, &tty);
  if (!enable) {
    tty.c_lflag &= ~ECHO;
  } else {
    tty.c_lflag |= ECHO;
  }
  tcsetattr(STDIN_FILENO, TCSANOW, &tty);
}

std::mutex cout_mtx;

}  // namespace

void PrintString(const std::string &s) {
  cout_mtx.lock();
  if (std::this_thread::get_id() != kThreadID) {
    std::cout << "\33[2K\r";
  }
  std::cout << s;
  if (std::this_thread::get_id() != kThreadID) {
    std::cout << last_print;
  }
  fflush(stdout);
  cout_mtx.unlock();
}

void PrintUnread() {
  cout_mtx.lock();
  std::cout << "--------UNREAD--------\n";
  fflush(stdout);
  cout_mtx.unlock();
}

void Break() { PrintString("\n"); }

void Error(const std::string &s) {
  cout_mtx.lock();
  if (std::this_thread::get_id() != kThreadID) {
    std::cout << "\33[2K\r";
    std::cout << last_print;
  }
  std::cout << s << "\n";
  if (std::this_thread::get_id() != kThreadID) {
    std::cout << last_print;
  }
  fflush(stdout);
  cout_mtx.unlock();
}

void PrintMessage(const cnmsg::UserStatus &user, const cnmsg::Message &msg,
                  bool reset) {
  cout_mtx.lock();
  if (reset && std::this_thread::get_id() != kThreadID) {
    std::cout << "\33[2K\r";
  }
  if (msg.type == cnmsg::ATTACH) {
    std::cout << "[" << user.GetUser(msg.user_id) << "][file] " << msg.msg
              << "\n";
  } else {
    std::cout << "[" << user.GetUser(msg.user_id) << "] " << msg.msg << "\n";
  }
  if (reset && std::this_thread::get_id() != kThreadID) {
    std::cout << last_print;
  }
  fflush(stdout);
  cout_mtx.unlock();
}

void ShowHelp() {
  cout_mtx.lock();
  std::cout << "register:\tregister\n";
  std::cout << "login:\t\tlogin\n";
  std::cout << "list-user:\tlist all users\n";
  std::cout << "list-chat:\tlist all chatrooms\n";
  std::cout << "send:\t\tsend message\n";
  std::cout << "send-file:\tsend file\n";
  std::cout << "get-file:\tdownload file\n";
  std::cout << "create:\t\tcreate chatroom\n";
  std::cout << "enter:\t\tenter chatroom\n";
  std::cout << "leave:\t\tleave chatroom\n";
  std::cout << "logout:\t\tlogout\n";
  std::cout << "unread:\t\tlist unread chats\n";
  std::cout << "clear:\t\tclear\n";
  std::cout << "exit:\t\tbye\n";
  fflush(stdout);
  cout_mtx.unlock();
}

void Clear() {
  cout_mtx.lock();
  std::cout << "\033[2J";
  std::cout << "\033[2H";
  cout_mtx.unlock();
}

std::string ReadString(cnmsg::UserStatus &user) {
  user.AckMessages();
  std::string s;
  if (!std::getline(std::cin, s)) exit(1);
  return s;
}

std::string ReadPassword(cnmsg::UserStatus &user, bool confirm) {
  user.AckMessages();
  PrintString(confirm ? "confirm: " : "password: ");
  SetStdinEcho(false);
  std::string s;
  if (!std::getline(std::cin, s)) exit(1);
  SetStdinEcho(true);
  return s;
}

std::string ReadCommand(cnmsg::UserStatus &user) {
  cout_mtx.lock();
  last_print = "";
  if (user.InChat()) last_print = "(" + user.GetChatRoom() + ")";
  last_print += "> ";
  std::cout << last_print;
  cout_mtx.unlock();
  return ReadString(user);
}

std::string ReadUsername(cnmsg::UserStatus &user) {
  PrintString("username: ");
  return ReadString(user);
}

std::string ReadMessage(cnmsg::UserStatus &user) {
  PrintString("message: ");
  return ReadString(user);
}

std::string ReadFilename(cnmsg::UserStatus &user) {
  PrintString("filename: ");
  return ReadString(user);
}

size_t ReadAttachID(cnmsg::UserStatus &user) {
  while (true) {
    PrintString("attachment ID: ");
    auto s = ReadString(user);
    bool valid = true;
    size_t attach_id = 0;
    for (auto c : s) {
      valid &= isdigit(c);
      attach_id = attach_id * 10 + (c - '0');
    }
    if (valid) return attach_id;
  }
}

size_t ReadChatID(cnmsg::UserStatus &user) {
  while (true) {
    PrintString("chat ID: ");
    auto s = ReadString(user);
    bool valid = true;
    size_t chat_id = 0;
    for (auto c : s) {
      valid &= isdigit(c);
      chat_id = chat_id * 10 + (c - '0');
    }
    if (valid) return chat_id;
  }
}

std::string ReadChatName(cnmsg::UserStatus &user) {
  PrintString("chat name: ");
  return ReadString(user);
}

std::string ReadUserIDs(cnmsg::UserStatus &user) {
  PrintString("user ID: ");
  return ReadString(user);
}

}  // namespace cli

namespace sha256 {

std::string Hash(const std::string &s) {
  std::string result;
  picosha2::hash256_hex_string(s, result);
  return result;
}

}  // namespace sha256

namespace base64 {

namespace {

inline bool IsBase64(char c) { return isalnum(c) || c == '+' || c == '/'; }

constexpr std::array<uint8_t, 256> BuildCharPos() {
  std::array<uint8_t, 256> res{};
  for (size_t i = 0; kBase64Char[i] != '\0'; ++i) res[kBase64Char[i]] = i;
  return res;
}

}  // namespace

constexpr std::array<uint8_t, 256> kBase64CharPos = BuildCharPos();

std::string Encode(const std::string &s) {
  std::string res;
  std::array<uint8_t, 3> u3{};
  std::array<uint8_t, 4> u4{};
  size_t len = ((s.size() + 2) / 3) * 3;
  for (size_t i = 0, j = 0; i < len; ++i) {
    u3[j++] = i < s.size() ? (uint8_t)s[i] : '\0';
    if (j == 3) {
      u4[0] = (u3[0] & 0xFC) >> 2;
      u4[1] = ((u3[0] & 0x03) << 4) + ((u3[1] & 0xF0) >> 4);
      u4[2] = ((u3[1] & 0x0F) << 2) + ((u3[2] & 0xC0) >> 6);
      u4[3] = u3[2] & 0x3F;
      for (size_t k = 0; k < 4; ++k) res += (char)kBase64Char[u4[k]];
      j = 0;
    }
  }
  for (size_t k = 0; k + s.size() < len; ++k) res[res.size() - k - 1] = '=';
  return res;
}

std::string Decode(const std::string &s) {
  std::string res;
  std::array<uint8_t, 3> u3{};
  std::array<uint8_t, 4> u4{};
  for (size_t i = 0, j = 0; i < s.size() && s[i] != '=' && IsBase64(s[i]);
       ++i) {
    u4[j++] = s[i];
    if (j == 4 || i + 1 == s.size() || s[i + 1] == '=') {
      for (size_t k = 0; k < 4; ++k) u4[k] = kBase64CharPos[u4[k]] & 0xFF;
      u3[0] = (u4[0] << 2) + ((u4[1] & 0x30) >> 4);
      u3[1] = ((u4[1] & 0xF) << 4) + ((u4[2] & 0x3C) >> 2);
      u3[2] = ((u4[2] & 0x3) << 6) + u4[3];
      for (size_t k = 0; k < j - 1; ++k) res += u3[k];
      j = 0;
    }
  }
  return res;
}

}  // namespace base64

namespace utils {

std::string ToString(const std::string &s) { return s; }
std::string ToString(char *s) { return std::string(s); }
std::string ToString(int s) { return std::to_string(s); }

std::string NoEscape(std::string s) {
  std::string res = "";
  for (size_t i = 0; i < s.size(); ++i) {
    if (s[i] == '\n') {
      res += "\\n";
    } else if (s[i] == '\r') {
      res += "\\r";
    } else {
      res += s[i];
    }
  }
  return res;
}

std::string CheckStartsWith(const std::string &s, const std::string &prefix) {
  assert(s.size() >= prefix.size() && s.substr(0, prefix.size()) == prefix);
  return s.substr(prefix.size());
}

std::vector<std::string> CutStrings(const std::string &s) {
  std::stringstream ss(s);
  std::vector<std::string> v;
  std::string t;
  while (ss >> t) v.push_back(t);
  return v;
}

std::string ParseAttach(const std::string &s) {
  size_t pos = s.find(':');
  assert(pos != std::string::npos);
  std::string filename = s.substr(pos + 1);
  return std::string("filename: ") + base64::Decode(filename) +
         " ID: " + s.substr(0, pos);
}

template <class Int>
Int ParseInt(const std::string &s) {
  Int v = 0;
  for (auto c : s) v = v * 10 + (c - '0');
  return v;
}

}  // namespace utils

namespace net {

Socket Connect(in_addr_t ip, uint16_t port) {
  sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = ip;
  int server_fd = 0;
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    std::cerr << "[Error] cannot create socket\n";
    exit(0);
  }
  if (connect(server_fd, reinterpret_cast<sockaddr *>(&server),
              sizeof(server)) < 0) {
    std::cerr << "[Error] fail to connect\n";
    exit(0);
  }
  SSL_CTX *ctx = ssl::InitCTX();
  SSL *ssl = SSL_new(ctx);
  SSL_set_fd(ssl, server_fd);
  if (SSL_connect(ssl) == -1) {
    std::cerr << "[Error] fail to SSL connect\n";
    exit(0);
  }
  return {server_fd, ssl, ctx};
}

template <class Head>
void SendMessage(const net::Socket &socket, Head &&head) {
  std::string message = utils::ToString(head) + "\n";
  if ((size_t)SSL_write(socket.ssl, message.data(), message.size()) !=
      message.size()) {
    std::cerr << "[Error] cannot send message\n";
    exit(0);
  }
}

template <class Head, class... Tail>
void SendMessage(const net::Socket &socket, Head &&head, Tail &&... tail) {
  std::string message = utils::ToString(head) +
                        ((std::string(" ") + utils::ToString(tail)) + ...) +
                        "\n";
  if ((size_t)SSL_write(socket.ssl, message.data(), message.size()) !=
      message.size()) {
    std::cerr << "[Error] cannot send message\n";
    exit(0);
  }
}

template <class Head, class... Tail>
std::pair<std::string, Socket> SendLoginMessage(Head &&head, Tail &&... tail) {
  auto socket = Connect(kServerIP, kServerPort);
  static char buf[kMaxBuffer];
  std::string message = utils::ToString(head) +
                        ((std::string(" ") + utils::ToString(tail)) + ...) +
                        "\n";
  if ((size_t)SSL_write(socket.ssl, message.data(), message.size()) !=
      message.size()) {
    std::cerr << "[Error] cannot send message\n";
    exit(0);
  }
  ssize_t sz;
  if ((sz = SSL_read(socket.ssl, buf, kMaxBuffer)) < 0) {
    std::cerr << "[Error] fail to receive message\n";
    exit(0);
  }
  std::string res(buf, buf + sz);
  res.pop_back();
  return std::make_pair(res, socket);
}

void SendFile(cnmsg::UserStatus &user, const std::string &filename,
              const std::string &file) {
  auto socket = Connect(kServerIP, kServerPort);
  auto username = user.GetUsername();
  auto password = user.GetPasswd();
  auto chat_id = user.GetChatID();
  std::thread thr(
      [username, password, chat_id, filename, file, s = std::move(socket)] {
        cli::PrintString("uploading " + filename + " in background\n");
        SendMessage(s, "send_file", username, password, chat_id,
                    base64::Encode(filename), base64::Encode(file));
        char buf[100];
        SSL_read(s.ssl, buf, 100);
        s.Close();
        cli::PrintString(filename + " uploaded\n");
      });
  thr.detach();
}

void GetFile(cnmsg::UserStatus &user, size_t attach_id,
             const std::string &file) {
  auto socket = Connect(kServerIP, kServerPort);
  auto username = user.GetUsername();
  auto password = user.GetPasswd();
  std::thread thr([username, password, attach_id, file, socket] {
    SendMessage(socket, "get_file", username, password, attach_id);
    static constexpr size_t kMaxBuffer = 4096;
    char buf[kMaxBuffer];
    std::ofstream ofs(file);
    ssize_t sz;
    while ((sz = SSL_read(socket.ssl, buf, kMaxBuffer)) > 0) {
      std::string str(buf, buf + sz);
      if (str == ";\n") {
        cli::Error("no such file");
        return;
      }
      bool brk = str.back() == '\n';
      str = base64::Decode(str);
      ofs.write(reinterpret_cast<char *>(str.data()), str.size());
      if (brk) break;
    }
    ofs.close();
    socket.Close();
    cli::PrintString(file + " downloaded\n");
  });
  thr.detach();
}

}  // namespace net

namespace cnmsg {

Message::Message(const std::string &s) {
  auto v = utils::CutStrings(s);
  assert(v.size() == 5);
  user_id = utils::ParseInt<size_t>(v[0]);
  chat_id = utils::ParseInt<size_t>(v[1]);
  msg_id = utils::ParseInt<size_t>(v[2]);
  type = v[4] == "text" ? TEXT : ATTACH;
  msg = type == ATTACH ? utils::ParseAttach(v[3]) : base64::Decode(v[3]);
}

void UserStatus::RcvMessage(const net::Socket &socket) {
  static constexpr size_t kMaxBuffer = 131072;
  static char buf[kMaxBuffer];
  while (keep_rcv_) {
    ssize_t sz = SSL_read(socket.ssl, buf, kMaxBuffer);
    if (sz < 0) continue;
    std::string res = std::string(buf, buf + sz);
    assert(res.back() == '\n');
    res.pop_back();
    size_t sp = res.find(' ');
    std::string type = sp == std::string::npos ? res : res.substr(0, sp);
    std::string msg = sp == std::string::npos ? "" : res.substr(sp + 1);
    CtrlMsgType msg_type = kMsgTypeTable.at(type);
    if (msg_type == RCV_MESSAGE) {
      auto g = Message(msg);
      if (g.chat_id == chat_id_) {
        PushAck(g.chat_id, g.msg_id);
        cli::PrintMessage(*this, g, true);
      }
      continue;
    }
    if (msg_type == CHAT_LIST) {
      if (UpdateChatList(msg) != user_id_) continue;
    }
    if (msg_type == USER_LIST) UpdateUserList(msg);
    {
      std::lock_guard<std::mutex> lk(msg_mtx_);
      msg_queue_.emplace(kMsgTypeTable.at(type), std::move(msg));
    }
    cv_.notify_one();
  }
}

void UserStatus::PushAck(size_t chat_id, size_t msg_id) {
  ack_mtx_.lock();
  ack_queue_.emplace(chat_id, msg_id);
  ack_mtx_.unlock();
}

void UserStatus::AckMessages() {
  if (!IsLoggedIn()) return;
  ack_mtx_.lock();
  while (!ack_queue_.empty()) {
    auto [chat_id, msg_id] = ack_queue_.front();
    ack_queue_.pop();
    net::SendMessage(socket_, "message_ack", chat_id, msg_id);
  }
  ack_mtx_.unlock();
}

bool UserStatus::Access(size_t chat_id) {
  if (!update_chat_) {
    net::SendMessage(socket_, "get_chats");
    WaitMessage(cnmsg::CHAT_LIST);
  }
  return id_to_chat_.find(chat_id) != id_to_chat_.end();
}

size_t UserStatus::UpdateChatList(const std::string &s) {
  chat_mtx_.lock();
  auto v = utils::CutStrings(s);
  size_t user_id = utils::ParseInt<size_t>(v[0]);
  chat_to_id_.clear();
  id_to_chat_.clear();
  for (size_t i = 1; i < v.size(); i += 2) {
    size_t id = utils::ParseInt<size_t>(v[i]);
    std::string chat_name = base64::Decode(v[i + 1]);
    chat_to_id_[chat_name] = id;
    id_to_chat_[id] = chat_name;
  }
  update_chat_ = true;
  chat_mtx_.unlock();
  return user_id;
}

void UserStatus::UpdateUserList(const std::string &s) {
  id_to_user_.clear();
  auto v = utils::CutStrings(s);
  for (size_t i = 0; i < v.size(); i += 2) {
    size_t id = utils::ParseInt<size_t>(v[i]);
    id_to_user_[id] = v[i + 1];
  }
}

void UserStatus::SetTimeout(int fd) {
  struct timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char *>(&tv),
             sizeof(tv));
}

std::string UserStatus::WaitMessage(CtrlMsgType type) {
  std::unique_lock<std::mutex> lk(msg_mtx_);
  cv_.wait(lk, [type, this] {
    return !msg_queue_.empty() && msg_queue_.front().type == type;
  });
  lk.unlock();
  cv_.notify_one();
  auto res = msg_queue_.front().msg;
  msg_queue_.pop();
  return res;
}

void UserStatus::Login(size_t user_id, net::Socket &&socket, std::string &&s,
                       std::string &&t) {
  user_id_ = user_id;
  socket_ = std::move(socket);
  username_ = std::move(s);
  passwd_ = std::move(t);
  keep_rcv_ = true;
  SetTimeout(socket_.fd);
  rcv_thread_ =
      std::thread([this, s = net::Socket(socket_)] { this->RcvMessage(s); });
}

void UserStatus::Logout() {
  AckMessages();
  keep_rcv_ = false;
  update_chat_ = false;
  rcv_thread_.join();
  socket_.Close();
  user_id_ = kNotLoggedIn;
  chat_id_ = kNoChatRoom;
}

size_t ParseLogin(std::string s) {
  static const std::string kLoginRes = "login_res";
  s = utils::CheckStartsWith(s, kLoginRes);
  size_t user_id = 0;
  for (size_t i = 0, j = 0; i < s.size(); ++i) {
    if (isdigit(s[i])) {
      if (j == 1) {
        user_id = user_id * 10 + (s[i] - '0');
      } else if (j == 2 && s[i] == '1') {
        return kFailed;
      }
    } else {
      assert(s[i] == ' ');
      j++;
    }
  }
  return user_id;
}

void HandleRegister(UserStatus &user) {
  if (user.IsLoggedIn()) return cli::Error("already logged in");
  auto username = cli::ReadUsername(user);
  std::string password, password2;
  while (true) {
    password = cli::ReadPassword(user);
    cli::Break();
    password2 = cli::ReadPassword(user, true);
    cli::Break();
    if (password == password2) break;
  }
  auto hashed = sha256::Hash(password + "880301");
  auto [msg, socket] = net::SendLoginMessage("register", username, hashed);
  size_t user_id = ParseLogin(msg);
  if (user_id == kFailed) return cli::Error("register failed");
  user.Login(user_id, std::move(socket), std::move(username),
             std::move(hashed));
  user.Access(0);
  HandleUnread(user);
}

void HandleLogin(UserStatus &user) {
  if (user.IsLoggedIn()) return cli::Error("already logged in");
  auto username = cli::ReadUsername(user);
  auto password = cli::ReadPassword(user);
  cli::Break();
  auto hashed = sha256::Hash(password + "880301");
  auto [msg, socket] = net::SendLoginMessage("login", username, hashed);
  size_t user_id = ParseLogin(msg);
  if (user_id == kFailed) return cli::Error("login failed");
  cli::PrintString("successfully logged in as " + username + "\n");
  user.Login(user_id, std::move(socket), std::move(username),
             std::move(hashed));
  user.Access(0);
  HandleUnread(user);
}

void HandleSend(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (!user.InChat()) return cli::Error("enter chatroom first");
  auto message = cli::ReadMessage(user);
  net::SendMessage(user.GetSocket(), "send_message", user.GetChatID(),
                   base64::Encode(message));
}

void HandleSendFile(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (!user.InChat()) return cli::Error("enter chatroom first");
  auto filename = cli::ReadFilename(user);
  std::ifstream ifs(filename);
  if (!ifs) return cli::Error("no such file");
  static char buf[kMaxFileSize];
  ifs.read(buf, sizeof(buf));
  std::string content(buf, buf + ifs.gcount());
  net::SendFile(user, filename, content);
}

void HandleGetFile(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (!user.InChat()) return cli::Error("enter chatroom first");
  size_t attach_id = cli::ReadAttachID(user);
  auto filename = cli::ReadFilename(user);
  net::GetFile(user, attach_id, filename);
}

void HandleEnter(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (user.InChat()) return cli::Error("leave chat first");
  size_t chat_id = cli::ReadChatID(user);
  if (!user.Access(chat_id)) return cli::Error("permission denied");
  user.SetChatID(chat_id);
  net::SendMessage(user.GetSocket(), "get_users");
  user.WaitMessage(cnmsg::USER_LIST);
  net::SendMessage(user.GetSocket(), "get_messages", chat_id);
  auto res = user.WaitMessage(cnmsg::MESSAGE_LIST);
  auto v = utils::CutStrings(res);
  assert(v.size() % 4 == 2);
  Message last_rcv;
  size_t last_ack_id = utils::ParseInt<size_t>(v[1]);
  std::vector<Message> msg_list;
  for (size_t i = 2; i < v.size(); i += 4) {
    auto msg = Message(v[i] + " " + std::to_string(chat_id) + " " + v[i + 1] +
                       " " + v[i + 2] + " " + v[i + 3]);
    msg_list.push_back(msg);
  }
  std::sort(msg_list.begin(), msg_list.end());
  bool unread = false;
  for (auto &msg : msg_list) {
    if (!unread && msg.msg_id > last_ack_id) {
      unread = true;
      cli::PrintUnread();
    }
    cli::PrintMessage(user, msg);
  }
  if (!msg_list.empty())
    user.PushAck(msg_list.back().chat_id, msg_list.back().msg_id);
}

void HandleLeave(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (!user.InChat()) return cli::Error("not in chat");
  user.LeaveChat();
}

void HandleListUser(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  net::SendMessage(user.GetSocket(), "get_users");
  auto res = user.WaitMessage(cnmsg::USER_LIST);
  auto v = utils::CutStrings(res);
  for (size_t i = 0; i < v.size(); i += 2) {
    cli::PrintString(std::string("id = ") + v[i] + " username = " + v[i + 1]);
    cli::Break();
  }
}

void HandleListChat(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  net::SendMessage(user.GetSocket(), "get_chats");
  user.WaitMessage(cnmsg::CHAT_LIST);
  for (auto it : user.GetChatList()) {
    cli::PrintString(std::string("id = ") + std::to_string(it.first) +
                     " chat name = " + it.second);
    cli::Break();
  }
}

void HandleLogout(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("not logged in");
  user.Logout();
  cli::PrintString("logout successfully\n");
}

void HandleCreate(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  auto chat_name = cli::ReadChatName(user);
  auto user_ids = cli::ReadUserIDs(user);
  net::SendMessage(user.GetSocket(), "create_chat", base64::Encode(chat_name),
                   user_ids);
  user.WaitMessage(cnmsg::CHAT_LIST);
}

void HandleUnread(UserStatus &user) {
  if (!user.IsLoggedIn()) return cli::Error("login first");
  if (user.InChat()) return cli::Error("leave chat first");
  net::SendMessage(user.GetSocket(), "get_unread_chats");
  auto res = user.WaitMessage(cnmsg::UNREAD_CHAT_LIST);
  auto v = utils::CutStrings(res);
  if (v.empty()) {
    cli::PrintString("no unread chats\n");
  } else {
    cli::PrintString("unread chats:\n");
    for (auto &s : v) {
      size_t chat_id = utils::ParseInt<size_t>(s);
      cli::PrintString(user.GetChatList().at(chat_id) + "(" + s + ")\n");
    }
  }
}

}  // namespace cnmsg

namespace ssl {

SSL_CTX *InitCTX() {
  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();
  SSL_CTX *ctx = SSL_CTX_new(SSLv23_client_method());
  if (!ctx) exit(1);
  return ctx;
}

}  // namespace ssl

int main() {
  cnmsg::UserStatus user;
  while (true) {
    auto cmd = cli::ReadCommand(user);
    if (cmd == "exit") {
      if (user.IsLoggedIn()) user.Logout();
      break;
    }
    if (cmd == "") continue;
    if (cmd == "help") {
      cli::ShowHelp();
    } else if (cmd == "register") {
      cnmsg::HandleRegister(user);
    } else if (cmd == "login") {
      cnmsg::HandleLogin(user);
    } else if (cmd == "send") {
      cnmsg::HandleSend(user);
    } else if (cmd == "send-file") {
      cnmsg::HandleSendFile(user);
    } else if (cmd == "get-file") {
      cnmsg::HandleGetFile(user);
    } else if (cmd == "list-user") {
      cnmsg::HandleListUser(user);
    } else if (cmd == "list-chat") {
      cnmsg::HandleListChat(user);
    } else if (cmd == "create") {
      cnmsg::HandleCreate(user);
    } else if (cmd == "enter") {
      cnmsg::HandleEnter(user);
    } else if (cmd == "leave") {
      cnmsg::HandleLeave(user);
    } else if (cmd == "logout") {
      cnmsg::HandleLogout(user);
    } else if (cmd == "unread") {
      cnmsg::HandleUnread(user);
    } else if (cmd == "clear") {
      cli::Clear();
    } else {
      cli::Error("invalid command");
    }
  }
  return 0;
}
