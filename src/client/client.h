#ifndef CLIENT_H_
#define CLIENT_H_

#include <arpa/inet.h>
#include <netinet/in.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <sys/socket.h>
#include <unistd.h>

#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

constexpr size_t kMaxFileSize = 20971520;

namespace net {

struct Socket {
  int fd;
  SSL *ssl;
  SSL_CTX *ctx;

  void Close() const {
    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(fd);
    SSL_CTX_free(ctx);
  }
};

}  // namespace net

namespace cnmsg {

enum CtrlMsgType {
  LOGIN_RES,
  CHAT_LIST,
  USER_LIST,
  MESSAGE_LIST,
  RCV_MESSAGE,
  UNREAD_CHAT_LIST
};
enum MsgType { TEXT, ATTACH };

static const std::unordered_map<std::string, CtrlMsgType> kMsgTypeTable = {
    {"user_list", USER_LIST},
    {"chat_list", CHAT_LIST},
    {"recv_message", RCV_MESSAGE},
    {"message_list", MESSAGE_LIST},
    {"unread_chat_list", UNREAD_CHAT_LIST}};

struct ControlMessage {
  CtrlMsgType type;
  std::string msg;

  ControlMessage() = default;
  ControlMessage(CtrlMsgType type, std::string &&s)
      : type(type), msg(std::move(s)) {}
};

struct Message {
  MsgType type;
  size_t user_id;
  size_t chat_id;
  size_t msg_id;
  std::string msg;

  Message() = default;
  Message(const std::string &s);

  bool operator<(const Message &rhs) const { return msg_id < rhs.msg_id; }
};

class UserStatus {
 private:
  static constexpr size_t kNotLoggedIn = (size_t)-1;
  static constexpr size_t kNoChatRoom = (size_t)-1;
  size_t user_id_;
  size_t chat_id_;
  net::Socket socket_;
  std::string username_, passwd_;
  std::thread rcv_thread_;
  std::queue<ControlMessage> msg_queue_;
  std::queue<std::pair<size_t, size_t>> ack_queue_;
  std::mutex msg_mtx_, ack_mtx_, chat_mtx_;
  std::condition_variable cv_;
  std::unordered_map<std::string, size_t> chat_to_id_;
  std::unordered_map<size_t, std::string> id_to_chat_;
  std::unordered_map<size_t, std::string> id_to_user_;
  bool keep_rcv_, update_chat_;

  void RcvMessage(const net::Socket &socket);
  void SetTimeout(int fd);
  size_t UpdateChatList(const std::string &s);
  void UpdateUserList(const std::string &s);

 public:
  UserStatus()
      : user_id_(kNotLoggedIn), chat_id_(kNoChatRoom), update_chat_(false) {}

  bool IsLoggedIn() const { return user_id_ != kNotLoggedIn; }
  bool InChat() const { return chat_id_ != kNoChatRoom; }
  void SetChatID(size_t chat_id) { chat_id_ = chat_id; }
  const net::Socket &GetSocket() const { return socket_; }
  size_t GetUserID() const { return user_id_; }
  size_t GetChatID() const { return chat_id_; }
  void LeaveChat() { chat_id_ = kNoChatRoom; }
  const std::string &GetUsername() const { return username_; }
  const std::string &GetPasswd() const { return passwd_; }
  const std::string &GetChatRoom() const { return id_to_chat_.at(chat_id_); }
  const std::string &GetUser(size_t user_id) const {
    return id_to_user_.at(user_id);
  }

  void AckMessages();
  bool Access(size_t chat_id);
  void PushAck(size_t chat_id, size_t msg_id);

  const std::unordered_map<size_t, std::string> &GetChatList() const {
    return id_to_chat_;
  }

  std::string WaitMessage(CtrlMsgType type);
  void Login(size_t user_id, net::Socket &&socket, std::string &&s,
             std::string &&t);
  void Logout();
};

constexpr size_t kFailed = (size_t)-1;

size_t ParseLogin(std::string s);
void HandleRegister(UserStatus &user);
void HandleLogin(UserStatus &user);
void HandleSend(UserStatus &user);
void HandleSendFile(UserStatus &user);
void HandleGetFile(UserStatus &user);
void HandleEnter(UserStatus &user);
void HandleLeave(UserStatus &user);
void HandleListUser(UserStatus &user);
void HandleListChat(UserStatus &user);
void HandleLogout(UserStatus &user);
void HandleCreate(UserStatus &user);
void HandleUnread(UserStatus &user);

}  // namespace cnmsg

namespace net {

constexpr size_t kMaxBuffer = 20971520;
constexpr size_t kNoFd = (size_t)-1;

Socket Connect(in_addr_t ip, uint16_t port);

template <class Head>
void SendMessage(const net::Socket &socket, Head &&head);

template <class Head, class... Tail>
void SendMessage(const net::Socket &socket, Head &&head, Tail &&... tail);

template <class Head, class... Tail>
std::pair<std::string, Socket> SendLoginMessage(Head &&head, Tail &&... tail);

void SendFile(cnmsg::UserStatus &user, const std::string &filename,
              const std::string &file);
void GetFile(cnmsg::UserStatus &user, size_t attach_id,
             const std::string &filename);

}  // namespace net

namespace utils {

std::string ToString(const std::string &s);
std::string ToString(int s);
std::string NoEscape(std::string s);
std::string CheckStartsWith(const std::string &s, const std::string &prefix);
std::vector<std::string> CutStrings(const std::string &s);
std::string ParseAttach(const std::string &s);

template <class Int>
Int ParseInt(const std::string &s);

}  // namespace utils

namespace base64 {

constexpr char kBase64Char[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

std::string Encode(const std::string &s);
std::string Decode(const std::string &s);

}  // namespace base64

namespace sha256 {

std::string Hash(const std::string &s);

}  // namespace sha256

namespace cli {

const std::thread::id kThreadID = std::this_thread::get_id();
std::string last_print;

void PrintString(const std::string &s);
void PrintUnread();
void Break();
void Error(const std::string &s);
void PrintMessage(const cnmsg::UserStatus &user, const cnmsg::Message &msg,
                  bool reset = false);
void ShowHelp();
void Clear();
std::string ReadString(cnmsg::UserStatus &user);
std::string ReadPassword(cnmsg::UserStatus &user, bool confirm = false);
std::string ReadCommand(cnmsg::UserStatus &user);
std::string ReadUsername(cnmsg::UserStatus &user);
std::string ReadMessage(cnmsg::UserStatus &user);
std::string ReadFilename(cnmsg::UserStatus &user);
size_t ReadAttachID(cnmsg::UserStatus &user);
size_t ReadChatID(cnmsg::UserStatus &user);
std::string ReadChatName(cnmsg::UserStatus &user);
std::string ReadUserIDs(cnmsg::UserStatus &user);

}  // namespace cli

namespace ssl {

SSL_CTX *InitCTX();

}  // namespace ssl

#endif  // CLIENT_H_
